#!/usr/bin/make -f
include /usr/share/dpkg/architecture.mk
include /usr/share/dpkg/buildflags.mk
include /usr/share/rustc/architecture.mk
export CFLAGS CXXFLAGS CPPFLAGS LDFLAGS
export DEB_HOST_RUST_TYPE DEB_HOST_GNU_TYPE
export CARGO_PROFILE_RELEASE_DEBUG=true
export SKIP_UTILS=stdbuf
VERSION := $(shell dpkg-parsechangelog -S Version|sed -e "s|.*~\(.*\)-.*|\1|g")
UPSTREAM_VERSION := $(shell dpkg-parsechangelog -S Version|cut -d- -f1)

# Build a multicall binary instead of multiple binaries
# to reduce the storage footprint
export MULTICALL = y

%:
	dh $@ --buildsystem cargo

override_dh_auto_test:
	# for now, don't fail the build if tests are failing
	CARGO_HOME=$(CURDIR)/debian/cargo_home make PROFILE=release MULTICALL=$(MULTICALL) test||true

override_dh_auto_install:
	CARGO_HOME=$(CURDIR)/debian/cargo_home DEB_CARGO_CRATE=coreutils_$(VERSION) /usr/share/cargo/bin/cargo prepare-debian debian/cargo_registry --link-from-system
#use thin rather than full lto on 32-bit to avoid running out of address space.
ifeq ($(DEB_HOST_ARCH_BITS),32)
	sed -i 's/lto = true/lto = "thin"/' Cargo.toml
endif
	CARGO_HOME=$(CURDIR)/debian/cargo_home DESTDIR=$(CURDIR)/debian/tmp/ make SELINUX_ENABLED=1 PROFILE=release MULTICALL=$(MULTICALL) install
ifeq ($(DEB_HOST_ARCH_BITS),32)
	sed -i 's/lto = "thin"/lto = true/' Cargo.toml
endif
# Create the symlink early to be able to generate the manpages
	cd debian/tmp/usr/share/zsh/site-functions && mkdir rust-coreutils && chmod -x _* && mv _* rust-coreutils
	cd debian/tmp/usr/share/bash-completion/completions/ && chmod -x rust-*
	cd debian/tmp/usr/share/fish/vendor_completions.d/ && chmod -x *.fish
	cd debian/tmp/usr/bin && mv rust-coreutils coreutils && rm rust-*
#	cd debian/tmp/usr/share/fish/vendor_completions.d && for f in *fish; do mv $$f rust-$$f; done
	dh_link

override_dh_missing:
	dh_missing --fail-missing

override_dh_dwz:
	# Don't do anything. fails because of the
	# https://github.com/rust-lang/rust/issues/66118

#use lower debuginfo level on 32-bit to avoid running out of address space.
ifeq ($(DEB_HOST_ARCH_BITS),32)
execute_after_dh_auto_configure:
	if test -f debian/cargo_home/config; then \
		sed -i s/debuginfo=2/debuginfo=1/ debian/cargo_home/config; \
	fi
endif

override_dh_clean:
	rm -f Cargo.toml.orig Cargo.lock
	dh_clean
