From: Arnaud Ferraris <arnaud.ferraris@collabora.com>
Date: Mon, 7 Jun 2021 18:13:18 +0200
Subject: HACK cp, mv: add dummy missing arguments

Post-installation scripts for `apparmor` and the kernel make use of
the `-Z` flag for `cp` or `mv` in order to set the SELinux context of
copied/moved files. As the current implementations don't support this
flag, the commands error out, causing those packages' installation to
fail. As those are required, building Apertis images using
`rust-coreutils` subsequently fails too.

This commit adds support for the `-Z` flag without implementing the
associated behavior as it isn't strictly needed for building basic
Apertis images.

Signed-off-by: Arnaud Ferraris <arnaud.ferraris@collabora.com>

Forwarded: not-needed
Bug: https://github.com/uutils/coreutils/issues/2404

--- a/src/uu/cp/src/cp.rs
+++ b/src/uu/cp/src/cp.rs
@@ -362,6 +362,7 @@
     pub const ATTRIBUTES_ONLY: &str = "attributes-only";
     pub const CLI_SYMBOLIC_LINKS: &str = "cli-symbolic-links";
     pub const CONTEXT: &str = "context";
+    pub const CONTEXT_DEFAULT: &str = "context-default";
     pub const COPY_CONTENTS: &str = "copy-contents";
     pub const DEREFERENCE: &str = "dereference";
     pub const FORCE: &str = "force";
@@ -658,6 +659,15 @@
                 .help(
                     "NotImplemented: set SELinux security context of destination file to \
                     default type",
+                ),
+        )
+        .arg(
+            Arg::new(options::CONTEXT_DEFAULT)
+                .short('Z')
+                .action(ArgAction::SetTrue)
+                .help(
+                    "NotImplemented: set SELinux security context of destination file to \
+                    default type",
                 ),
         )
         // END TODO
--- a/src/uu/mv/src/mv.rs
+++ b/src/uu/mv/src/mv.rs
@@ -107,6 +107,7 @@
 static OPT_TARGET_DIRECTORY: &str = "target-directory";
 static OPT_NO_TARGET_DIRECTORY: &str = "no-target-directory";
 static OPT_VERBOSE: &str = "verbose";
+static OPT_CONTEXT: &str = "context";
 static OPT_PROGRESS: &str = "progress";
 static ARG_FILES: &str = "files";
 
@@ -256,6 +257,18 @@
                 .value_parser(ValueParser::os_string())
                 .value_hint(clap::ValueHint::AnyPath),
         )
+    // TODO: implement the following args
+        .arg(
+            Arg::new(OPT_CONTEXT)
+                .short('Z')
+                .long(OPT_CONTEXT)
+                .action(ArgAction::SetTrue)
+                .help(
+                    "NotImplemented: set SELinux security context of destination file \
+                       to default type",
+                )
+        )
+    // END TODO
 }
 
 fn determine_overwrite_mode(matches: &ArgMatches) -> OverwriteMode {
